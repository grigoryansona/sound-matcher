import { Component, OnInit } from '@angular/core';
import { SoundsInterface } from './interfaces/sounds.interface';
import { AppService } from './services/app.service';
import { MatTableDataSource } from '@angular/material/table';
import * as _ from 'lodash';
import { MatDialog } from '@angular/material/dialog';
import { ActionDialogComponent } from './dialogs/action-dialog/action-dialog.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'sound-matcher';
  displayedColumns: string[] = ['artist', 'title', 'isrc', 'duration'];
  soundsFromDatabase: SoundsInterface[] = [];
  soundsForInput: SoundsInterface[] = [];
  dataSource!: MatTableDataSource<SoundsInterface>;
  showTable: boolean = true;
  showAddBtn: boolean = false;
  selectedSound: any;
  soundSelector: any;
  constructor(
              private appService: AppService,
              public dialog: MatDialog
  ) {
     
  }
  
  ngOnInit() {
    this.getSounds()
    this.getSoundsForInput()
  }

  public getSounds() {
    this.soundsFromDatabase = this.appService.getSounds()
    this.dataSource = new MatTableDataSource(this.soundsFromDatabase);
  }

  public getSoundsForInput() {
    this.soundsForInput = this.appService.getSoundsforInput()
  }

  onChange($event: any) {
    this.selectedSound = $event.value
    if(this.selectedSound) {
      let filteredData = _.filter(this.soundsFromDatabase,(item) =>{
        return item.artist ===  $event.value.artist && item.title ===  $event.value.title && item.isrc ===  $event.value.isrc && item.duration ===  $event.value.duration;
      })
      if(filteredData.length) {
        this.showTable = true
        this.dataSource = new MatTableDataSource(filteredData)
      } else {
        this.showTable = false
        this.openDialog()
      }
    } else {
      this.dataSource = new MatTableDataSource(this.soundsFromDatabase)
    }
  }

  openDialog() {
    const dialogRef = this.dialog.open(ActionDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      switch (result) {
        case 'add':
          this.addSound()
          break

        case 'search':
          this.searchGlobally()
          break  
      }
    });
  }

  public addSound() {
    this.showTable = true
    this.showAddBtn = false
    this.soundSelector = ''
    this.soundsFromDatabase.push(this.selectedSound)
    this.dataSource = new MatTableDataSource(this.soundsFromDatabase)
  }

  public searchGlobally() {
    let filteredData = _.filter(this.soundsFromDatabase,(item) =>{
      return item.artist ===  this.selectedSound.artist;
    })
    if(filteredData.length) {
      this.showTable = true
      this.showAddBtn = true
      this.dataSource = new MatTableDataSource(filteredData)
    } else {
      this.showTable = false
      this.openDialog()
    }
  }
}
