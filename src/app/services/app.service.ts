import { Injectable } from '@angular/core';
import { SoundsInterface } from '../interfaces/sounds.interface';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  soundsFromDatabase$: SoundsInterface[] = [
    {
      artist: 'Ed Sheeran',
      title: 'Shape of You',
      isrc: 'GBAHS1600463',
      duration: '233'
    },
    {
      artist: 'Ed Sheeran',
      title: 'Shape of You (Latin Remix)',
      isrc: 'GBAHS1700245',
      duration: ''
    },
    {
      artist: 'Muse',
      title: 'Supremacy',
      isrc: 'GBAHT1200389',
      duration: '295'
    },
    {
      artist: 'Muse',
      title: 'Supremacy',
      isrc: 'GBAHT1500513',
      duration: ''
    },
    {
      artist: 'Muse',
      title: 'Supremacy (Live At Rome Olympic Stadium)',
      isrc: 'GBAHT1326121',
      duration: ''
    },
    {
      artist: 'Miles Davis',
      title: 'Freddie Freeloader',
      isrc: 'USSM15900114',
      duration: '586'
    },
    {
      artist: 'Miles Davis',
      title: 'Freddie Freeloader',
      isrc: 'FR6V81367550',
      duration: '423'
    },
    {
      artist: 'Miles Davis',
      title: 'Freddie Freeloader',
      isrc: 'DEPZ69005640',
      duration: '583'
    },
    {
      artist: 'Miles Davis',
      title: 'Freddie Freeloader (Album Version)',
      isrc: '',
      duration: ''
    }
  ]

  soundsForInput$: SoundsInterface[] = [
    {
      artist: 'Ed Sheeran',
      title: 'Shape of You',
      isrc: '',
      duration: '233'
    },
    {
      artist: 'Ed Sheeran',
      title: 'Shape of You (Latin Remix)',
      isrc: 'GBAHS1700245',
      duration: '237'
    },
    {
      artist: 'Ed Sheeran with Kranium, Nyla, Major Lazer',
      title: 'Shape of You',
      isrc: 'GBAHS1700228',
      duration: '237'
    },
    {
      artist: 'Ed Sheeran',
      title: 'Shape of You (Acoustic)',
      isrc: '',
      duration: '223'
    },
    {
      artist: 'Muse',
      title: 'Supremacy',
      isrc: 'GBAHT1200389',
      duration: '295'
    },
    {
      artist: 'Muse',
      title: 'Supremacy',
      isrc: 'GBAHT1500513',
      duration: ''
    },
    {
      artist: 'Muse',
      title: 'Supremacy',
      isrc: 'GBAHT1326121',
      duration: ''
    },
    {
      artist: 'Miles Davis',
      title: 'Freddie Freeloader',
      isrc: 'USSM15900114',
      duration: '580'
    },
    {
      artist: 'Miles Davis',
      title: 'Freddie Freeloader',
      isrc: 'FR6V81367550',
      duration: ''
    },
    {
      artist: 'Miles Davis',
      title: 'Freddie Freeloader',
      isrc: 'DEPZ69005640',
      duration: '583'
    },
    {
      artist: 'Miles Davis',
      title: 'Freddie Freeloader (Album Version)',
      isrc: 'USSM15900114',
      duration: '401'
    }
  ]

  constructor() { }

  public getSounds() {
    return this.soundsFromDatabase$;
  }

  public getSoundsforInput() {
    return this.soundsForInput$;
  }
}
