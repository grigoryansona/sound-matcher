import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-action-dialog',
  templateUrl: './action-dialog.component.html',
  styleUrls: ['./action-dialog.component.scss']
})
export class ActionDialogComponent implements OnInit {

  constructor(private dialogRef: MatDialogRef<ActionDialogComponent>) { }

  ngOnInit(): void {
  }

  onAction(event: string) {
    this.dialogRef.close(event);
  }
}
