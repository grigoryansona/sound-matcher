export interface SoundsInterface {
    artist: string,
    title: string,
    isrc: string,
    duration: string,
}